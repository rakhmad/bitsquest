// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
 
// MIT license
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// END MIT Code

(function(NS) {
  var Obj = NS.Obj;

  var TICK_TIMEOUT = 50;
  var RENDER_RATE = 2;

  var intersect = function(a, b, extendB) {
    var x1 = a.p1.x, x2 = a.p2.x, x3 = b.p1.x, x4 = b.p2.x;
    var y1 = a.p1.y, y2 = a.p2.y, y3 = b.p1.y, y4 = b.p2.y;
    var denom =  ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
    var ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denom;
    var ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denom;
    if ((ua >= 0 && ua <= 1) && ((ub >= 0 && ub <= 1) || (ub > 0 && extendB))) {
      s = ua;
      var intersection = {x: x1 + s * (x2 - x1), y: y1 + s * (y2 - y1)};
      var d = Math.pow(intersection.x - b.p1.x, 2) + Math.pow(intersection.y - b.p1.y, 2);
      return { i: intersection, d: d };
    }
    return undefined;
  };

  var World = function(contexts, level) {
    var i, obj;

    this.stopped = true;
    this.contexts = contexts;
    this.level = level;

    this.objects = new Box();

    this.tickCount = 0;

    this.moveable = [];
    this.particles = [];
    this.objectMap = {};

    for (i = 0; i < level.walls.length; i++) {
      var wall = level.walls[i];
      this.createWall(wall[0], wall[1], wall[2], wall[3]);
    }

    if (level.objects) {
      for (i = 0; i < level.objects.length; i++) {
        var objdef = level.objects[i];

        if (objdef.hasOwnProperty('type')) {
          obj = new NS[objdef.type](objdef);
          obj.world = this;
        } else {
          obj = this.createObj(objdef.box[0], objdef.box[1], objdef.box[2], objdef.box[3], objdef);
        }

        this.addObject(obj);
      }
    }

    if (level.init) {
      level.init(this);
    }

    this._renderBackground(contexts.background);

    this.tick = this.tick.bind(this);
    window.console.time = this.tickCount;
    window.console.clear();
  };

  World.prototype = {
    removeObject: function(obj) {
      var i = 0;
      if (obj.name) {
        delete this.objectMap[obj.name];
      }
      if (obj.moveable) {
        i = this.moveable.length - 1;
        while (i >= 0) {
          if (this.moveable[i].id === obj.id) {
            this.moveable.splice(i, 1);
            break;
          }
          i--;
        }
      }
    },

    addObject: function(obj) {
      if (obj.name) {
        this.objectMap[obj.name] = obj;
      }
      if (obj.interactive) {
        this.objects.addChild(obj);
      }
      if (obj.moveable) {
        this.moveable.push(obj);
      }
    },
    deconstructVisibleBoxes: function() {
      var visitor = {
        segments: [],
        visit: function(box) {
          if (!box.invisible && !box.passable) {
            // concat is probably expensive.
            this.segments = this.segments.concat(box.decompose());
          }
        }
      };
      this.objects.visit(visitor);
      return visitor.segments;
    },

    _getRelevantPoints: function(x,y, allSegments) {
      var dot = function(p1, p2) {
        return p1.x * p2.x + p1.y * p2.y;
      };

      var i;
      var points = [];
      var segments = [];
      var segmentId = 0;
      var angle = 0;
      for (i = 0; i < allSegments.length; i++) {
        var segment = allSegments[i];
        var dotValue  = dot({ x: segment.midpoint.x - x, y: segment.midpoint.y - y}, segment.normal);

        if (dotValue < 0) {
          segments.push(segment);
          segmentId ++;
          var p1 = { p: segment.p1, s: segment, id: segmentId, dot: dotValue };
          var p2 = { p: segment.p2, s: segment, id: segmentId, dot: dotValue };

          p1.angle = Math.atan2(p1.p.y - y, p1.p.x - x);
          p2.angle = Math.atan2(p2.p.y - y, p2.p.x - x);

          var dAngle = p2.angle - p1.angle;
          if (dAngle <= -Math.PI) { dAngle += 2*Math.PI; }
          if (dAngle > Math.PI) { dAngle -= 2*Math.PI; }
          p1.begin = (dAngle > 0.0);
          p2.begin = !p1.begin;

          points.push(p1);
          points.push(p2);
        }
      }

      points.sort(function(p1, p2) {
        var result = p1.angle - p2.angle;
        if (result === 0)  {
          if (p1.begin !== p2.begin) {
            result = p1.begin ? -1 : 1;
          } else {
            return 0;
          }
        }
        return result;
      });

      return points;
    },

    createObj: function(fromX, fromY, toX, toY, properties) {
      var x1, x2, y1, y2;

      if (fromX > toX) {
        x1 = toX;
        x2 = fromX;
      } else {
        x1 = fromX;
        x2 = toX;
      }
      if (fromY > toY) {
        y1 = toY;
        y2 = fromY;
      } else {
        y1 = fromY;
        y2 = toY;
      }

      var width = x2 - x1;
      var height = y2 - y1;

      var obj = new Obj({ width: width, height: height, world: this });
      obj = _.extend(obj, properties);
      obj.transform = obj.translate(x1 + width / 2, y1 + height / 2);
      obj._update();

      return obj;
    },

    lose: function() {
      // function overriden
      this.stopped = true;
    },

    win: function() {
      // function overriden
      this.stopped = true;
    },

    findObject: function(name) {
      return this.objectMap[name];
    },

    _drawWall: function(context) {
      var hwidth = this.width / 2;
      var hheight = this.height / 2;
      context.save();
      context.beginPath();
      context.fillStyle = '#205081';
      context.strokeStyle = '#205081';
      context.rect(-hwidth, -hheight, this.width, this.height);
      context.stroke();
      context.fill();
      context.restore();

    },
    createWall: function(fromX, fromY, toX, toY) {
      var wall = this.createObj(fromX, fromY, toX, toY, { passable: false, invisible: false });
      wall.draw = this._drawWall;
      this.objects.addChild(wall);
    },

    _renderBackground: function(context) {
      var i;

      context.fillStyle = '#ffffff';
      context.fillRect(0,0,600,600);

      for (i = 1; i < 60; i++) {
        context.beginPath();
        if (i % 10 === 0) {
          context.strokeStyle = '#a4b3c2';
        } else {
          context.strokeStyle = '#f6f7f9';
        }
        context.moveTo(0, i * 10 - 1);
        context.lineTo(600, i * 10 - 1);
        context.moveTo(i * 10 - 1, 0);
        context.lineTo(i * 10 - 1, 600);
        context.stroke();
      }
    },
    nearest: function(ray) {
      var i;
      var segments = this.deconstructVisibleBoxes();
      var nearest;
      for (i = 0; i < segments.length; i++) {
        var segment = segments[i];
        var inter = intersect(segment, ray, true);
        if (inter) {
          if (!nearest) {
            nearest = inter;
          } else {
            if (nearest.d > inter.d) {
              nearest = inter;
            }
          }
        }
      }
      return nearest;
    },

    render: function() {
      var overlayContext = this.contexts.overlay;
      var context = this.contexts.world;
      overlayContext.clearRect(0,0,600,600);
      context.clearRect(0,0,600,600);
      this.objects.render(context);
      var particles = this.particles;
      for (i = 0; i < particles.length; i++) {
        this.particles[i].render(context);
      }
    },

    addParticle: function(particle) {
      this.particles.push(particle);
    },

    removeParticle: function(particle) {
      var particles = this.particles;

      for (var i = 0; i < particles.length; i++) {
        var p = particles[i];
        if (p.id === particle.id) {
          particles.splice(i, 1);
          this.particles = particles;
          break;
        }
      }
    },

    tick:  function() {
      setTimeout(this.tick, TICK_TIMEOUT);

      var world = this;
      var m, i, j, p;
      if (this.stopped) {
        return;
      }
      if (console.setTime) {
        console.setTime(this.tickCount);
      }
      try {
        this.objects.visit({
          visit: function(obj) {
            if (obj.tick) {
              obj.tick(world);
            }
          }
        });

        for (m = 0; m < this.moveable.length; m++) {
          var moveable = this.moveable[m];

          var objects = this.objects.intersects(moveable);
          for (i = 0; i < objects.length; i++) {
            var object = objects[i];
            var parts = moveable.intersects(object);

            for (j = 0; j < parts.length; j++) {
              var part = parts[j];
              if (part.onContact) {
                part.onContact.call(part, object, moveable);
              }
            }

            if (object.onContact) {
              object.onContact.call(object, moveable, this);
            }
            if (object.onCovered && moveable.overlays(object)) {
              object.onCovered.call(object, moveable, this); 
            }

            if (this.stopped) {
              return;
            }
          }
        }

        var particles = this.particles;
        i = particles.length - 1;
        while (i >= 0) {
          this.particles[i].tick(this);
          i--;
        }
      } catch (e) {
        this.stopped = true;
        console.log(e);
        if (console.display) {
          console.display();
        }
        return;
      }

      if (console.display) {
        console.display();
      }

      this.tickCount++;
      if ((this.tickCount % RENDER_RATE) === 0) {
        window.requestAnimationFrame(this.render.bind(this));
      }
    },

    start: function() {
      this.stopped = false;
      this.tick();
    }
  };


  var LitWorld = function() {
    World.call(this);

    this.lights = new Box();

    this.lighting = true;
    this.darkness = 0.80;

    // TODO: Create these programmatically
    this.workarea = document.getElementById('work_area').getContext('2d');
    this.gradient = document.getElementById('gradient').getContext('2d');
    this.gradient.clearRect(0,0,310,310);

    var g = this.gradient.createRadialGradient(155, 155, 0, 155, 155, 155);
    g.addColorStop(0,   'rgba(0,0,0,0)'); // 100%
    g.addColorStop(0.180, 'rgba(0,0,0,0.20)'); // 25% 
    g.addColorStop(0.250, 'rgba(0,0,0,0.23)'); // 25% 
    g.addColorStop(0.400, 'rgba(0,0,0,0.50)'); // 10%
    g.addColorStop(0.600,   'rgba(0,0,0,0.750)'); // 6%
    g.addColorStop(1,   'rgba(0,0,0,1)'); // 1%
    this.gradient.setTransform(1, 0, 0, 1, 0.5, 0.5);
    this.gradient.fillStyle = g;
    this.gradient.fillRect(0,0, 310, 310);
  };

  /**
   * Lighting inspired by this post: http://www.redblobgames.com/articles/visibility/
   */
  LitWorld.prototype = _.defaults({
    _illuminate: function(obj) {
      if (obj.illuminate) {
        obj.illuminate(this);
      }
    },

    _renderLighting: function(context, shadowContext) {
      var i;
      if (this.lighting) {
        this.lights.visit({ visit: this._illuminate.bind(this) });
        // doin it, doin it, and doin it manually
        var imageData = context.getImageData(0,0,599,599);
        var data1 = imageData.data;
        var data2 = shadowContext.getImageData(0,0,599,599).data;

        for (i = 0; i < data1.length; i++) {
          data1[i] = (data1[i] * data2[i]) >> 8;
        }
        context.putImageData(imageData, 0, 0);
      }
    },
    addObject: function(obj) {
      World.prototype.addObject.call(this, obj);

      if (obj.illuminate) {
        this.lights.addChild(obj);
      }
    },


    calculateLight: function(x, y) {
      var triangles = [];
      var allSegments = this.visibleSegments;

      var i,j,k;

      var points = this._getRelevantPoints(x,y, allSegments); 
      var open = [];
      var previousEnd;
      var nearest;
      var next;

      for (k = 0; k < 2; k++)  {
        for (i = 0; i < points.length; i++) {
          var point = points[i];
          var start;
          var inter;
          var ray = { p1: { x: x, y: y}, p2: { x: point.p.x, y: point.p.y}};
          var current_old = open.length > 0 ? open[0] : null;

          if (point.begin) {
            start = intersect(point.s, ray);
            point.inter = start;
            nearest = undefined;
            next = undefined;
            for (j = 0; j < open.length; j++) {
              next = open[j];
              inter = intersect(next.s, ray, true);
              next.inter = inter;
              if (start.d < inter.d) {
                nearest = next;
                break;
              } 

              if (start.d === inter.d) {
                var dmp1 = Math.pow(point.s.midpoint.x - x, 2) + Math.pow(point.s.midpoint.y - y, 2);
                var dmp2 = Math.pow(next.s.midpoint.x - x, 2) + Math.pow(next.s.midpoint.y - y, 2);
                if (dmp2 > dmp1) {
                  nearest = next;
                  break;
                }
              }
            }
            if (! nearest) {
              open.push(point);
            } else {
              open.splice(j, 0, point);
            }
          } else {
            for (j = 0; j < open.length; j++) {
              if (open[j].id === point.id) {
                break;
              }
            }
            if (j < open.length) {
              // Copy endpoint intersection to begin object for rendering
              var removed = open.splice(j, 1)[0];
              //what did I uncover?
              start = intersect(point.s, ray);
              removed.inter = start;

              if (j === 0) {
                next = open[0];
                if (next) {
                  inter = intersect(next.s, ray, true);
                  next.inter = inter;
                  next.start = inter;
                }
              }
            }
          }

          if (open.length > 0 && open[0].start === undefined) {
            open[0].start = open[0].inter;
          }

          var current_new = open.length > 0 ? open[0] : null;

          if (current_old != current_new) {
            if (current_old != null && current_old.start) {
              if (k === 1) {
                triangles.push({
                  p1: { x: x, y: y },
                  p2: { x: current_old.start.i.x, y: current_old.start.i.y },
                  p3: { x: current_old.inter.i.x, y: current_old.inter.i.y }
                });
              }
              current_old.start = undefined;
            }
          }
          angle = point.angle;
        }
      }
      return triangles;
    },

    drawLighting: function(realX, realY, color) {
      if (!this.lighting) { 
        return;
      }
      var i;
      var context = this.workarea;
      context.setTransform(1, 0, 0, 1, 0.5, 0.5);
      context.clearRect(0,0,300,300);
      context.fillStyle = color;
      context.strokeStyle = color;

      var triangles = this.calculateLight(realX, realY);

      for (i = 0; i < triangles.length; i++) {
        var t = triangles[i];
        context.beginPath();
        context.moveTo(t.p1.x - realX + 150, t.p1.y - realY + 150);
        context.lineTo(t.p2.x - realX + 150, t.p2.y - realY + 150);
        context.lineTo(t.p3.x - realX + 150, t.p3.y - realY + 150);
        context.lineTo(t.p1.x - realX + 150, t.p1.y - realY + 150);
        context.fill();
        context.stroke();
      }
      context.save();
      context.globalCompositeOperation = 'destination-out';
      context.drawImage(this.gradient.canvas, -5, -5);
      context.restore();

      var shadowContext = this.contexts.shadow;
      shadowContext.save();
      shadowContext.globalCompositeOperation = 'lighter'; 
      shadowContext.drawImage(context.canvas, realX - 150, realY - 150);
      shadowContext.restore();
    },

    render: function() {
      var overlayContext = this.contexts.overlay;
      var context = this.contexts.world;

      overlayContext.clearRect(0,0,600,600);

      var visibleSegments = this.deconstructVisibleBoxes();
      visibleSegments.push(new Box.prototype._Segment({ x: -80, y: 680 }, { x: -80, y: -80 }, undefined));
      visibleSegments.push(new Box.prototype._Segment({ x: -80, y: -80 }, { x: 680, y: -80 }, undefined));
      visibleSegments.push(new Box.prototype._Segment({ x: 680, y: -80 }, { x: 680, y: 680 }, undefined));
      visibleSegments.push(new Box.prototype._Segment({ x: 680, y: 680 }, { x: -80, y: 680 }, undefined));
      this.visibleSegments = visibleSegments;

      var shadowContext = this.contexts.shadow;
      var d = 255 - ((255 * this.darkness) | 0);

      shadowContext.fillStyle = 'rgb(' + d + ',' + d + ',' + d + ')';

      shadowContext.fillRect(0,0,599,599);
      shadowContext.setTransform(1,0,0,1,0.5,0.5);

      this._renderBackground(context);
      this.objects.render(context);

      this._renderLighting(context, shadowContext);
    },

    setDarkness: function(darkness) {
      this.darkness = darkness;
      this.lighting = darkness > 0.01;
    }
  }, World.prototype);

  NS.World = World;
}(window));
