Bit's Quest
=========

![Bit's Quest](http://f.cl.ly/items/0E352K1s172X1j2O0m2k/bits_quest.png)

Program your way through 15 levels.

![](http://f.cl.ly/items/3t1T1e1E0B3m2s2A0p0g/Screen%20Shot%202013-07-09%20at%205.57.42%20PM.png)

Play
----

[http://bitsquest.bitbucket.org/](http://bitsquest.bitbucket.org/)


Improve
------

Fair warning: It's a mess.

Things that could use improvement:

* More levels
* Use a real physics library
* Re-enable light rendering
* Improve error handling
* Proper partitioning of the render graph.
* Additional languages
* Everything else -- a complete rewrite would probably help. :)

Questions, comments, concerns, contact dbennett@atlassian.com or ddbennett on twitter.


Everything not already covered under a separate license (i.e., jquery, underscore, backbone, codemirror) is released under the APL v2 (see LICENSE file for official notice).
